# Azure IoTHub Connector

## Description
Azure IoTHub Connector Plugin for the Reekoh IoT Platform.

## Configuration
The Azure IoTHub Connector Plugin configuration can be done once you've created your own pipeline in Reekoh. To configure the plugin, you will be asked to provide the following details:
- **Connector Name** - This is a label given to your plugin to locate it easily in your pipeline.
- **Protocol** - Client Transport protocol

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/azure-iothub-connector/1.0.0/azure-iothub-configure.png)

## Send Data

In order to simulate sending data, You will need a Gateway Plugin to send the data to Azure IoTHub Connector Plugin. In the screenshot below, it uses HTTP Gateway Plugin. Note: Look for the documentation on how to use this plugin.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/azure-iothub-connector/1.0.0/azure-iothub-pipeline.png)

Make sure your plugins and pipeline are successfully deployed.

Using **POST MAN** as HTTP Client simulator, you can now simulate sending data.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/azure-iothub-connector/1.0.0/azure-iothub-postman.png)

These parameters are then injected to the plugin from the platform.

## Sample input data
The HTTP Gateway only accepts data in JSON format. Also, a "device" field is required to be in the request body. This device field should contain a device ID which is registered in Reekoh's Devices Management. The example below is data to be sent.

```
{
    "device":"SMART_METER_890",
	"temperature": 75,
    "windSpeed": 25,
    "humidity": 16
}
```

## Verify Data

The data will be ingested by the HTTP Gateway plugin, which will be forwarded to all the other plugins that are connected to it in the pipeline.

To verify if the data is ingested properly in the pipeline, you need to check the **LOGS** tab in every plugin in the pipeline. All the data that have passed through the plugin will be logged in the **LOGS** tab.
If an error occurs, an error exception will be logged in the **EXCEPTIONS** tab.

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/azure-iothub-connector/1.0.0/azure-iothub-logs.png)

You can also verify if the data ingested properly by checking in your Azure Account Dashboard. Another way of checking the data is to use Windows PowerShell then type "iothub-explorer monitor-events --login "Azure Connection String" and press enter."

![](https://reekohdocs.blob.core.windows.net/plugin-docs/connector/azure-iothub-connector/1.0.0/azure-iothub-verify.png)