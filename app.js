
'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Connector()
/* eslint-disable new-cap */
let rkhLogger = new reekoh.logger('azure-iothub-connector')
let isEmpty = require('lodash.isempty')
let isPlainObject = require('lodash.isplainobject')
let get = require('lodash.get')

let Client = require('azure-iot-device').Client
let Message = require('azure-iot-device').Message

let commsObject = null
let protocol = null
let connectionString = null

plugin.on('data', (data) => {
  if (!isPlainObject(data)) {
    return plugin.logException(new Error('Invalid data. Data should be a valid JSON Object.'))
  }
  if (isEmpty(data)) {
    return plugin.logException(new Error('Invalid data. Data should not be empty.'))
  }

  connectionString = get(data.rkhDeviceInfo.metadata, 'azureConnectionString')
  if (isEmpty(connectionString)) {
    return plugin.logException(new Error('Invalid Connection String. Connection String should not be empty.'))
  }
  commsObject = Client.fromConnectionString(connectionString, protocol)

  let connectCloud = function (err) {
    if (err) {
      console.log(err)
      rkhLogger.error(err)
      return plugin.logException(err)
    } else {
      data = JSON.stringify(data)
      let message = new Message(data)
      commsObject.sendEvent(message)
    }
    plugin.log({
      title: 'Data sent to IoTHub Connector',
      data: data
    })
  }
  commsObject.open(connectCloud)

  plugin.log({
    title: 'Data received from IoTHub Connector',
    data: data
  })
})

plugin.once('ready', () => {
  if (plugin.config.protocol === 'http') {
    protocol = require('azure-iot-device-http').Http
  } else if (plugin.config.protocol === 'mqtt') {
    protocol = require('azure-iot-device-mqtt').Mqtt
  } else {
    protocol = require('azure-iot-device-amqp').Amqp
  }
  plugin.log('Azure IoTHub Connector has been initialized.')
  rkhLogger.info(`Azure IoTHub Connector has been initialized.`)
  plugin.emit('init')
})

module.exports = plugin
