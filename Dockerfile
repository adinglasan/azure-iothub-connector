FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/azure-iothub-connector

WORKDIR /home/node/azure-iothub-connector

# Install dependencies
# RUN npm install pm2@2.6.1 -g

# CMD pm2-docker --json app.yml
CMD ["node", "app.js"]
